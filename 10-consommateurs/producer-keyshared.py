#!/usr/bin/python3

import pulsar,random

client = pulsar.Client('pulsar://pulsar1:6650')

producer = client.create_producer('persistent://xtenant/xns/xavki-shared-2',                  
                                    properties={
                                        "producer-name": "test-producer-name",
                                        "producer-id": "test-producer-id"
                                    })

for i in range(1000000):
    key = random.randint(1, 10)
    producer.send(('Hello-%d' % i).encode('utf-8'),partition_key=str(key))
    print(key)
client.close()

