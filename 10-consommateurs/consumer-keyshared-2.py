#!/usr/bin/python3

import pulsar

client = pulsar.Client('pulsar://192.168.13.170:6650')

consumer = client.subscribe(topic='persistent://xtenant/xns/xavki-shared-2', subscription_name='sub4',initial_position=pulsar.InitialPosition.Earliest,consumer_type=pulsar.ConsumerType.KeyShared,key_shared_policy=pulsar.ConsumerKeySharedPolicy(key_shared_mode=pulsar.KeySharedMode.Sticky,sticky_ranges=[(32001,65535)],allow_out_of_order_delivery=True))

while True:
    msg = consumer.receive()
    try:
        print("Received message '{}' id='{}' key='{}'".format(msg.data(), msg.message_id(), msg.partition_key()))
        # Acknowledge successful processing of the message
        consumer.acknowledge(msg)
    except Exception:
        # Message failed to be processed
        consumer.negative_acknowledge(msg)

client.close()

