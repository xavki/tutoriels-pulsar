%title: Pulsar
%author: xavki



██████╗ ██╗   ██╗██╗     ███████╗ █████╗ ██████╗ 
██╔══██╗██║   ██║██║     ██╔════╝██╔══██╗██╔══██╗
██████╔╝██║   ██║██║     ███████╗███████║██████╔╝
██╔═══╝ ██║   ██║██║     ╚════██║██╔══██║██╔══██╗
██║     ╚██████╔╝███████╗███████║██║  ██║██║  ██║
╚═╝      ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝
                                                 



-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Définitions & Concepts

<br>


Messages

	* éléments transmis entre producteur et consommateur

	* constitution : 
			key/value, propriétés, producer name, schema version
			sequenceID (producer), messageID (bookies)
			publish time, event time (optional)

	* format : json, avro, protobuf, binaire...
			convention entre P & C

	* schéma : structure du message & optionnel

	* acknowledgment ou acquittement

	* retry, redelivery

	* compression : lz4, zlib, zstd, snappy


-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Définitions & Concepts

<br>

Producers

	* créé le message et le transmets

	* distribution au(x) bon(s) topic(s)

	* mode de vérification de la livraison (acquittement)

	* spécification de la partition

	* gestion des erreurs

-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Définitions & Concepts

<br>

Consumers

	* souscription à un ou plusieurs topics (pattern)

	* dépendance de la vitesse de consommation

	* déplacement du cursor suivant le mode de souscription

-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Définitions & Concepts

<br>

Topics

	* couche logique (similaire à une table en DB)

	* regroupement de messages du même contexte

	* ordonné

	* typé : persistent ou non

	* contenu dans tenant + namespace

	{type}://{tenant}/{namespace}/{topic}

	* découpé en sous éléments de distribution = partitions
		logique

-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Définitions & Concepts

<br>

Cursor

	* similaire à l'offset de kafka

	* un jalon qui marque l'état d'avancement d'une consommation

	* lag = différence entre le dernier élément produit et consommé

-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Définitions & Concepts

<br>

Brokers

	* serveurs permettant les connexions producer et consumer

	* interface avec les services de stockage (bookies)

	* sans état = scalabilité et disponibilité

	* communication : configuration zk / load balancing / coordination / cache

	* 2 sous services : webserver API REST + server TCP

	* disposent de managed ledger


-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Définitions & Concepts

<br>

Bookies

	* service bookeeper

	* WAL distribué (Write Ahead Log)

	* met à disposition les ledgers (brique de stockage)

	* très efficace pour de la données séquentielles (ça tombe bien hein !!)

	* répartition de la charge I/O et potentiellement lecture/écriture (disks diff)

	* scalable
