
docker run --name client-beijing  --hostname beijing -it --rm  --network test_docker_pulsar_pulsar apachepulsar/pulsar-test-latest-version:latest /bin/bash

docker run --name client-shanghai  --hostname shanghai -it --rm  --network test_docker_pulsar_pulsar apachepulsar/pulsar-test-latest-version:latest /bin/bash

docker run --name client-guangzhou  --hostname guangzhou -it --rm  --network test_docker_pulsar_pulsar apachepulsar/pulsar-test-latest-version:latest /bin/bash


########################################################################

#beijing - client settings

sed -i "s/localhost/broker-beijing/g" conf/client.conf
./bin/pulsar-admin clusters list

#shanghai - client settings

sed -i "s/localhost/broker-shanghai/g" conf/client.conf

#guangzhou - client settings

sed -i "s/localhost/broker-guangzhou/g" conf/client.conf


########################################################################

#beijing - create clusters

bin/pulsar-admin clusters create --url http://broker-shanghai:8080 --broker-url pulsar://broker-shanghai:6650 shanghai
bin/pulsar-admin clusters create --url http://broker-guangzhou:8080 --broker-url pulsar://broker-guangzhou:6650 guangzhou

#shanghai - create clusters

bin/pulsar-admin clusters create --url http://broker-guangzhou:8080 --broker-url pulsar://broker-guangzhou:6650 guangzhou
bin/pulsar-admin clusters create --url http://broker-beijing:8080 --broker-url pulsar://broker-beijing:6650 beijing

#guangzhou - create clusters

bin/pulsar-admin clusters create --url http://broker-shanghai:8080 --broker-url pulsar://broker-shanghai:6650 shanghai
bin/pulsar-admin clusters create --url http://broker-beijing:8080 --broker-url pulsar://broker-beijing:6650 beijing


########################################################################


#ALL - create tenants/namespaces

bin/pulsar-admin tenants create my-tenant  --allowed-clusters beijing,shanghai,guangzhou
bin/pulsar-admin namespaces create my-tenant/my-namespace --clusters beijing,shanghai,guangzhou

########################################################################

#beijing - produce messages

bin/pulsar-client produce  my-tenant/my-namespace/t1  --messages "hello-from-beijing-1" -n 10

bin/pulsar-client consume -s "sub-guangzhou" my-tenant/my-namespace/t1 -n 0

bin/pulsar-client consume -s "sub-shanghai" my-tenant/my-namespace/t1 -n 0

