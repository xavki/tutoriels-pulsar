%title: Pulsar
%author: xavki



██████╗ ██╗   ██╗██╗     ███████╗ █████╗ ██████╗ 
██╔══██╗██║   ██║██║     ██╔════╝██╔══██╗██╔══██╗
██████╔╝██║   ██║██║     ███████╗███████║██████╔╝
██╔═══╝ ██║   ██║██║     ╚════██║██╔══██║██╔══██╗
██║     ╚██████╔╝███████╗███████║██║  ██║██║  ██║
╚═╝      ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝
                                                 



-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Introduction - c'est quoi ??

<br>

* Système de messageries applicatif / gestion asynchrone

	producteurs > brokers > consommateurs

<br>

* Origines :

	* Yahoo

	* opensource 2016 > fondation Apache

<br>

* Concurrents : zeromq, rabbitmq, kafka

<br>

* Prend en charge à la fois :

	* queueing (consommation direct)

	* streaming (commitlog)

-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Introduction - c'est quoi ??


<br>

Sites officiels : 

	* https://pulsar.apache.org/

	* https://github.com/apache/pulsar

	* managés : streamnative + datastax

<br>

* Système distribué, scaling et haute disponibilité

<br>

* Fragmentation des services :

		* management des messages

		* stockage


* Offres plus d'options + Scaler avec précisions

-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Introduction - c'est quoi ??


<br>

* Deux principaux types de services :

	* brokers

	* bookies (bookeeper)

<br>

* et leur copain !!

	* zookeeper : configurations & topics

-----------------------------------------------------------------------------------------------------------                                          

# Pulsar : Introduction - c'est quoi ??


Kafka en mieux...

<br>

* non affectation de ressources à des topics non utilisés

	* nombre de topics illimités

<br>

* tiering : classes de stockage différentes (ssd, hdd, s3)

<br>

* isolation :

	* tenants

	* namespaces

<br>

* géoréplication... merci zookeeper
